﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraConterller : MonoBehaviour
{
    public GameObject Player;
    Vector3 norm;

    // Start is called before the first frame update
    void Start()
    {
        norm = transform.position - Player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void LateUpdate()
    {
        transform.position = Player.transform.position + norm;
    }
}
