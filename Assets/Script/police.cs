﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class police : MonoBehaviour
{
    AudioSource audios;
    public AudioClip ses;
    public Transform Player;
    protected NavMeshAgent PoliceMesh;
    Vector3 norm;
    float follow;
    // Start is called before the first frame update
    void Start()
    {
        audios = GetComponent<AudioSource>();
        PoliceMesh = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        norm = transform.position - Player.transform.position;
        //Debug.Log(norm.magnitude);
        follow = norm.magnitude;
        if (follow <= 1.6f)
        {         
            PoliceMesh.SetDestination(Player.position);
            sound();
        }
       
    }
    public void sound()
    {
        if (audios.isPlaying)
        {
            return;
        }
        audios.PlayOneShot(ses);
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("police aktif");
            SceneManager.LoadScene("Level1");
        }
    }
}


