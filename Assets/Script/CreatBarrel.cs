﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreatBarrel : MonoBehaviour
{
    public List<BarrelScript> Barrelspool;
    int BarrelInstate = 25;
    public BarrelScript Barrel;
    public Vector3 Randompos;
    public GameObject Player;
    public GameObject Restart;
    public GameObject Exit;
    public GameObject Joyistick;
    public GameObject Jump;

    public void Awake()
    {
        Barrelspool = new List<BarrelScript>();
        for (int i = 0; i < BarrelInstate; i++)
        {
            BarrelScript newBarrelpool = Instantiate(Barrel);
            newBarrelpool.gameObject.SetActive(false);
            Barrelspool.Add(newBarrelpool);
        }
    }
    void Start()
    {
    }
    private void Update()
    {
        if (Player == false)
        {
            Restart.SetActive(true);
            Exit.SetActive(true);
            Joyistick.SetActive(false);
            Jump.SetActive(false);
        }
    }
    public void gamerestart()
    {
        SceneManager.LoadScene("Menü");
    }
    public void gameexit()
    {
        Application.Quit();
    }

}
