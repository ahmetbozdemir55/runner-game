﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;
public class PlayerScript : MonoBehaviour
{
    public Joystick joystick;
    public Button ButtonSpace;
    private enum ControlMode
    {
        /// <summary>
        /// Up moves the character forward, left and right turn the character gradually and down moves the character backwards
        /// </summary>
        Tank,
        /// <summary>
        /// Character freely moves in the chosen direction from the perspective of the camera
        /// </summary>
        Direct
    }

    [SerializeField] private float m_moveSpeed = 2;
    [SerializeField] private float m_turnSpeed = 200;
    [SerializeField] private float m_jumpForce = 4;

    [SerializeField] private Animator m_animator;
    [SerializeField] private Rigidbody m_rigidBody = null;

    [SerializeField] private ControlMode m_controlMode = ControlMode.Direct;

    private float m_currentV = 0;
    private float m_currentH = 0;
    public float Forward = 25.0f;
    public Rigidbody rb;
    private readonly float m_interpolation = 10;
    private readonly float m_walkScale = 0.33f;
    private readonly float m_backwardsWalkScale = 0.16f;
    private readonly float m_backwardRunScale = 0.66f;
    private bool m_wasGrounded;
    private Vector3 m_currentDirection = Vector3.zero;
    public GameObject FailScene;
    public GameObject SkorText;
    public GameObject Player;
    private float m_jumpTimeStamp = 0;
    private float m_minJumpInterval = 0.25f;
    private bool m_jumpInput = false;
    private bool m_isGrounded;
    public Animator animator;
    private List<Collider> m_collisions = new List<Collider>();
    RaycastHit nesne;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        joystick = FindObjectOfType<Joystick>();
        Physics.gravity = new Vector3(0, -30, 0);
        ButtonSpace.onClick.AddListener(JumpButton);
    }

    // Update is called once per frame
    void Update()
    {
        var rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = new Vector3(joystick.Horizontal * 3f, rigidbody.velocity.y, joystick.Vertical * 3f);
        float Xposition = Mathf.Clamp(transform.position.x, -90, 90);
        float Zposition = Mathf.Clamp(transform.position.z, -30, 130);
        transform.position = new Vector3(Xposition, transform.position.y, Zposition);
        if (joystick.Horizontal==0 && joystick.Vertical==0)
        {
            animator.SetBool("Walk", false);
        }
        else
        {
            animator.SetBool("Walk", true);
        }
    }
    public void JumpButton()
    {

        if (!m_jumpInput)
        {
            m_jumpInput = true;
            rb = GetComponent<Rigidbody>();
            rb.velocity = Vector3.up * Time.deltaTime * Forward;
            Invoke("JumpFinish", 0.25f);
            animator.SetBool("JumpManuel", true);
        }
    }
    public void JumpFinish()
    {
        m_jumpInput = false;
        animator.SetBool("JumpManuel", false);
    }
    //private void Awake()
    //{
    //    if (!m_animator) { gameObject.GetComponent<Animator>(); }
    //    if (!m_rigidBody) { gameObject.GetComponent<Animator>(); }
    //}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "stone")
        {

            FailScene.SetActive(true);
            SkorText.SetActive(false);
            gameObject.SetActive(false);
        }

        //        ContactPoint[] contactPoints = collision.contacts;
        //        for (int i = 0; i < contactPoints.Length; i++)
        //        {
        //            if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
        //            {
        //                if (!m_collisions.Contains(collision.collider))
        //                {
        //                    m_collisions.Add(collision.collider);
        //                }
        //                m_isGrounded = true;
        //            }
        //        }
        //    }

        //    private void OnCollisionStay(Collision collision)
        //    {
        //        ContactPoint[] contactPoints = collision.contacts;
        //        bool validSurfaceNormal = false;
        //        for (int i = 0; i < contactPoints.Length; i++)
        //        {
        //            if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
        //            {
        //                validSurfaceNormal = true; break;
        //            }
        //        }

        //        if (validSurfaceNormal)
        //        {
        //            m_isGrounded = true;
        //            if (!m_collisions.Contains(collision.collider))
        //            {
        //                m_collisions.Add(collision.collider);
        //            }
        //        }
        //        else
        //        {
        //            if (m_collisions.Contains(collision.collider))
        //            {
        //                m_collisions.Remove(collision.collider);
        //            }
        //            if (m_collisions.Count == 0) { m_isGrounded = false; }
        //        }
        //    }

        //    private void OnCollisionExit(Collision collision)
        //    {
        //        if (m_collisions.Contains(collision.collider))
        //        {
        //            m_collisions.Remove(collision.collider);
        //        }
        //        if (m_collisions.Count == 0) { m_isGrounded = false; }
        //    }

        //    private void FixedUpdate()
        //    {
        //        m_animator.SetBool("Grounded", m_isGrounded);

        //        switch (m_controlMode)
        //        {
        //            case ControlMode.Direct:
        //                DirectUpdate();
        //                break;

        //            case ControlMode.Tank:
        //                TankUpdate();
        //                break;

        //            default:
        //                Debug.LogError("Unsupported state");
        //                break;
        //        }

        //        m_wasGrounded = m_isGrounded;
        //        //m_jumpInput = false;
        //    }

        //    private void TankUpdate()
        //    {
        //        float v = Input.GetAxis("Vertical");
        //        float h = Input.GetAxis("Horizontal");

        //        bool walk = Input.GetKey(KeyCode.LeftShift);

        //        if (v < 0)
        //        {
        //            if (walk) { v *= m_backwardsWalkScale; }
        //            else { v *= m_backwardRunScale; }
        //        }
        //        else if (walk)
        //        {
        //            v *= m_walkScale;
        //        }

        //        m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
        //        m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);

        //        transform.position += transform.forward * m_currentV * m_moveSpeed * Time.deltaTime;
        //        transform.Rotate(0, m_currentH * m_turnSpeed * Time.deltaTime, 0);

        //        m_animator.SetFloat("MoveSpeed", m_currentV);

        //        JumpingAndLanding();
        //    }

        //    private void DirectUpdate()
        //    {
        //        float v = Input.GetAxis("Vertical");
        //        float h = Input.GetAxis("Horizontal");

        //        Transform camera = Camera.main.transform;

        //        if (Input.GetKey(KeyCode.LeftShift))
        //        {
        //            v *= m_walkScale;
        //            h *= m_walkScale;
        //        }

        //        m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
        //        m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);

        //        Vector3 direction = camera.forward * m_currentV + camera.right * m_currentH;

        //        float directionLength = direction.magnitude;
        //        direction.y = 0;
        //        direction = direction.normalized * directionLength;

        //        if (direction != Vector3.zero)
        //        {
        //            m_currentDirection = Vector3.Slerp(m_currentDirection, direction, Time.deltaTime * m_interpolation);

        //            transform.rotation = Quaternion.LookRotation(m_currentDirection);
        //            transform.position += m_currentDirection * m_moveSpeed * Time.deltaTime;

        //            m_animator.SetFloat("MoveSpeed", direction.magnitude);
        //        }

        //        JumpingAndLanding();
        //    }


        //    private void JumpingAndLanding()
        //    {
        //        bool jumpCooldownOver = (Time.time - m_jumpTimeStamp) >= m_minJumpInterval;

        //        if (jumpCooldownOver && m_isGrounded && m_jumpInput)
        //        {
        //            m_jumpTimeStamp = Time.time;
        //            m_rigidBody.AddForce(Vector3.up * m_jumpForce, ForceMode.Impulse);
        //        }

        //        if (!m_wasGrounded && m_isGrounded)
        //        {
        //            m_animator.SetTrigger("Land");
        //        }

        //        if (!m_isGrounded && m_wasGrounded)
        //        {
        //            m_animator.SetTrigger("Jump");
        //        }
        //    }
        //}
    }
}
