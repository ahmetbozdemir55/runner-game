﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Stonedown : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Stone;
    public Vector3 Randompos;
    public GameObject RestartButton;
    public Joystick joystick;
    public Button ButtonSpace;
    private int k;
    Scene scene;
    void Start()
    {
        StartCoroutine(Instantiate());
        scene = SceneManager.GetActiveScene();
        //Debug.Log("Active Scene name is: " + scene.name + "\nActive Scene index: " + scene.buildIndex);
        k = scene.buildIndex;

    }

    IEnumerator Instantiate()
    {
        yield return new WaitForSeconds(1);
            for (int i = 0; i < 12; i++)
            {
                Vector3 vec = new Vector3(4.75f, 7, -60);
                Instantiate(Stone, vec, Quaternion.identity);
                yield return new WaitForSeconds(1);
            Debug.Log(i);
                if (i > 2)
                {
                Debug.Log("if içi" + i);
                    RestartButton.SetActive(true);
                }
            }
          
    }
    // Update is called once per frame 
    void Update()
    {
        joystick.gameObject.SetActive(false);
        ButtonSpace.gameObject.SetActive(false);
    }
    public void RButton()
    {
        Debug.Log("GİRDİ");
        SceneManager.LoadScene(k);
    }

}
