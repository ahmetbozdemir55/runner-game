﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameController : MonoBehaviour
{
    public GameObject Player;
    public Text StoryText;
    private int k;
    Scene scene;
    public List<ParticleSystem> successParticles;
    public PlayerGold countercall;
    public GameObject counterGetir;
    public GameObject Door;
    public GameObject Confetti;
    public GameObject CreatBarrelCall;
    List<BarrelScript> newcreatbarrelpool;
    public int Timecounterdown=35;
    public TextMeshProUGUI Timecounter;
    public TextMeshProUGUI rundoor;
    public GameObject ResartButton;
    // Start is called before the first frame update
    void Start()
    {  
        scene = SceneManager.GetActiveScene();
        k = scene.buildIndex;
        countercall = counterGetir.GetComponent<PlayerGold>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        if (countercall.counter >= 3)
        {
            Confetti.SetActive(true);
            Door.transform.DOMoveX(2, 15);
            newcreatbarrelpool = CreatBarrelCall.GetComponent<CreatBarrel>().Barrelspool;
            StartCoroutine(Test());
            rundoor.gameObject.SetActive(true);
            Timecounter.gameObject.SetActive(true);
            StartCoroutine(CounterDown());
            if (Timecounterdown<1)
            {
                ResartButton.gameObject.SetActive(true);
            }
        }
    }
    public IEnumerator CounterDown()
    {
        while (Timecounterdown > 0)
        {
            Timecounter.text = "0:" + Timecounterdown.ToString();
            yield return new WaitForSeconds(0.8f);
            Timecounterdown--;
        }
        Timecounter.text = "";
    }
    public IEnumerator Test()
    {
        for (int i = 0; i < 26; i++)
           
        {
            newcreatbarrelpool[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
        }
    }
    public void PlayConfetti()
    {
        foreach (var item in successParticles)
        {
            item.gameObject.SetActive(true);
            item.Play();
        }
    }
        public void NextButton()
    {
        SceneManager.LoadScene(k);
    }
}
