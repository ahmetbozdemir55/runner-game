﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelScript : MonoBehaviour
{
    public float thrust= 0.5f;
    public Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {  
     gameObject.transform.position = new Vector3(Random.Range(-1.5f, 1.5f), 4, (86));
    }

    // Update is called once per frame
    void Update()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(0, 0, -thrust, ForceMode.Impulse);
    }
    public void OnCollisionEnter(Collision collision)
    {
            if (collision.gameObject.tag == "Barrel" && collision.gameObject.tag == "Box")
            {
                Destroy(GameObject.FindWithTag("Barrel"));
            }
    }
}
