﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
public class hareket : MonoBehaviour
{
    //globalde tanımlaması gereken tüm değişkenler start metoduna kadar tanılandı
    public NavMeshAgent followup;
    public GameObject player;
    AudioSource audios;
    public AudioClip ses;
    Text oyunbittitext;
    Vector3 vec;
    float fark;
    public GameObject oyunbittipanel;
    void Start()
    { //örümceğe ait ses dosyasını çalmak için eklenen audio componentinin çağrıldığı kod

        audios = GetComponent<AudioSource>();
    }
    void Update()
    { //örümcek ve player objeleri arasında ki mesafenin hesaplanması için yazdığım kod
        followup.SetDestination(player.transform.position);
        fark = Vector3.Distance(player.transform.position, followup.transform.position);
        if (fark <= 1.66)
        { //aradaki fark vektör olarak hesaplanıp 1,66dan küçük olduğunda örümceğin sesinin
          //duyulması için yazdığım kod satırı
            audios.PlayOneShot(ses);
        }
    }
    void OnTriggerEnter(Collider yokol)
    {
        //çalışan sahneyi zamanını sıfırlayarak durdurduğum kod satırı
        Time.timeScale = 0;
        //örümcek player a değdiğinde yakalanılmış olacağında oyun bitirilecek örümceği yok
        //ettiğim kod satırı
        Destroy(followup);
        //Başarısız olunmuş olan bu senaryo için aktif olmayan oyun bitti paneli çalıştırılır.
        oyunbittipanel.SetActive(true);
    }

}