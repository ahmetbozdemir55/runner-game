﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    public GameObject Stone;
    int k=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (k==0)
            {
                StartCoroutine(Instantiate());
            }

        }

    }
    IEnumerator Instantiate()
    {


            for (int i = 0; i < 4; i++)
            {
                Vector3 vec = new Vector3(0, 4, 6);
                Instantiate(Stone, vec, Quaternion.identity);
                yield return new WaitForSeconds(1);
            k= k + 1;
        }
        for (int i = 0; i < 4; i++)
        {
            Vector3 vec = new Vector3(-2, 4, 6);
            Instantiate(Stone, vec, Quaternion.identity);
            yield return new WaitForSeconds(1);
            k = k + 1;
        }
        for (int i = 0; i < 4; i++)
        {
            Vector3 vec = new Vector3(0,3, 8);
            Instantiate(Stone, vec, Quaternion.identity);
            yield return new WaitForSeconds(1);
            k = k + 1;
        }
        for (int i = 0; i < 4; i++)
        {
            Vector3 vec = new Vector3(2, 4, 12);
            Instantiate(Stone, vec, Quaternion.identity);
            yield return new WaitForSeconds(1);
            k = k + 1;
        }
        for (int i = 0; i < 4; i++)
        {
            Vector3 vec = new Vector3(-2, 4, 12);
            Instantiate(Stone, vec, Quaternion.identity);
            yield return new WaitForSeconds(1);
            k = k + 1;
        }
        for (int i = 0; i < 4; i++)
        {
            Vector3 vec = new Vector3(2, 4, 16);
            Instantiate(Stone, vec, Quaternion.identity);
            yield return new WaitForSeconds(1);
            k = k + 1;
        }
        for (int i = 0; i < 4; i++)
        {
            Vector3 vec = new Vector3(-2, 4, 16);
            Instantiate(Stone, vec, Quaternion.identity);
            yield return new WaitForSeconds(1);
            k = k + 1;
        }
        for (int i = 0; i < 4; i++)
        {
            Vector3 vec = new Vector3(0, 4,20);
            Instantiate(Stone, vec, Quaternion.identity);
            yield return new WaitForSeconds(1);
            k = k + 1;
        }
    }
}
